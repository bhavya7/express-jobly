# Jobly Backend


This is the Express backend for Jobly.

**To understand the tasks, visit [this file](task.md)**

To run this:

    node server.js
    
To run the tests:

    jest -i

