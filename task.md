# **Jobly-Backend**


## **Goals & Requirements**

- This is a pure API app, taking values from the query string (GET requests) or from a JSON body (other requests). It returns JSON.
- This gets authentication/authorization with JWT tokens. Make sure your additions only allow access as specified in our requirements.
- Be thoughtful about function and variable names, and write developer-friendly documentation *for every function and route* you write.
- The starter code is well-tested, with excellent coverage.


---

## **Part One: Setup / Starter Code**

Fork this repo for the starter code. 

Do a quick skim of the code to get a sense of the main components and the organization.

We’ve provided ***jobly.sql***, which will create a database (with a small amount of starter data) and a test database. Set those up. (Some of the tables included are not currently used by the application; you’ll add the parts of the app that will use those tables in the exercise).

Start up the server (note that, unlike most exercises, we start this server on port 3001).

Test the API in Insomnia/Postman.


### **First Task: sqlForPartialUpdate**

A starting piece to document:
```
We’ve provided a useful method in helpers/sql.js called sqlForPartialUpdate. This code works, and we use it, but the code is undocumented and not directly tested. Thoroughly document the function.
```

---
## **Part Two: Companies**

We’ve provided a model and routes for companies.

### **Adding Filtering**
The route for listing all companies (**GET /companies**) works, but it currently shows all companies. Add a new feature to this, allowing API users to filter the results based on optional filtering criteria, any or all of which can be passed in the query string:

- ***name***: filter by company name: if the string “net” is passed in, this should find any company who name contains the word “net”, case-insensitive (so “Study Networks” should be included).
- ***minEmployees***: filter to companies that have at least that number of employees.
- ***maxEmployees***: filter to companies that have no more than that number of employees.
- If the ***minEmployees*** parameter is greater than the ***maxEmployees*** parameter, respond with a 400 error with an appropriate message.

Some requirements:

- Do not solve this by issuing a more complex SELECT statement than is needed (for example, if the user isn’t filtering by ***minEmployees*** or ***maxEmployees***, the SELECT statement should not include anything about the ***num_employees***.

- Validate that the request does not contain inappropriate other filtering fields in the route. Do the actual filtering in the model.

- Document all new code here clearly; this is functionality that future team members should be able to understand how to use from your docstrings.

---

## **Part Three: Change Authorization**

Many routes for this site do not have appropriate authorization checks.

### **Companies**
- Retrieving the list of companies or information about a company should remain open to everyone, including anonymous users.
- Creating, updating, and deleting companies should only be possible for users who logged in with an account that has the ***is_admin*** flag in the database.

Find a way to do this where you don’t need to change the code of these routes, and where you don’t need to SELECT information about the user on every request, but that the authentication credentials provided by the user can contain information suitable for this requirement.

Update tests to demonstrate that these security changes are working.

### **Users**
- Creating users should only permitted by admins (registration, however, should remain open to everyone).
- Getting the list of all users should only be permitted by admins.
- Getting information on a user, updating, or deleting a user should only be permitted either by an admin, or by that user.

---

## **Part Four: Jobs**

Add a feature for jobs to the application.

We’ve already provided a table for this. Study it.

### **Adding Job Model, Routes, and Tests**
Add a model for jobs — you can pattern-match this from the companies model.

Updating a job should never change the ID of a job, nor the company associated with a job.

Add routes for jobs. The same routes should be handled as we did for companies (for now, omit the special filtering on the ***GET /*** route), with the same security requirements (anyone can get the jobs, but only admins can add, update, or delete them). Make sure you suitably validate incoming data.


### **Adding Filtering**
Similar to the companies filtering for the ***GET /*** route, add filtering for jobs for the following possible filters:

- ***title***: filter by job title. Like before, this should be a case-insensitive, matches-any-part-of-string search.
- ***minSalary***: filter to jobs with at least that salary.
- ***hasEquity***: if ***true***, filter to jobs that provide a non-zero amount of equity. If ***false*** or not included in the filtering, list all jobs regardless of equity.

Document this feature well.

### **Show Jobs for a Company**
Now that the app includes jobs, change the ***GET /companies/:handle*** feature so that it includes all of the information about the jobs associated with that company:
```
{ ... other data ... , jobs: [ { id, title, salary, equity}, ... ] }
```

---

## **Step Five: Job Applications**

We’ve provided a table for applications. Incorporate this into the app by adding a method onto the User model, allowing users to apply for a job.

Add a route at POST /users/:username/jobs/:id that allows that user to apply for a job (or an admin to do it for them). That route should return JSON like:
```
{ applied: jobId }
```

Change the output of the get-all-info methods and routes for users so those include the a field with a simple list of job IDs the user has applied for:
```
{ ..., jobs: [ jobId, jobId, ... ] }
```